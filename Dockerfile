# Use uma imagem base Node.js
FROM node:14

# Defina o diretório de trabalho dentro do contêiner
WORKDIR /app

# Copie o arquivo package.json e package-lock.json para o diretório de trabalho
COPY package*.json ./

# Execute o comando npm install para instalar as dependências da aplicação
RUN npm install

# Copie o código-fonte da aplicação para o diretório de trabalho
COPY . .

# Exponha a porta em que a aplicação irá rodar (substitua a porta pelo valor correto)
EXPOSE 3000

# Inicie a aplicação
CMD [ "node", "app.js" ]

