const deviceStatus = {
    temperature: null,
    humidity: null,
    capacity: null,
    lastUpdated: null
};

export default deviceStatus;