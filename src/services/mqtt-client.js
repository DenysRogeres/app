import mqtt from 'mqtt'
import deviceStatus from './device-status.js'

const protocol = 'mqtt'
const host = 'public.mqtthq.com'
// const username = 'pi2-umidificador'
// const password = 'pi2-umidificador'
const port = '1883'
const clientId = `mqtt_${Math.random().toString(16).slice(3)}`

const connectUrl = `${protocol}://${host}:${port}`

const client = mqtt.connect(connectUrl, {
    clientId: clientId,
    clean: true,
    connectTimeout: 4000,
    reconnectPeriod: 1000,
    // username,
    // password
});

client.on('connect', () => {
    console.log('connected');
    client.subscribe('status', (err) => {
        if (err) {
            console.log(err);
        }
    })
    client.subscribe('getStatus', (err) => {
        if (err) {
            console.log(err);
        }
    })
    client.publish('getStatus', 'teste');
    setInterval(() => {
        client.publish('getStatus', 'teste1');
    }, 60000);
});

client.on('message', (topic, message) => {
    if (topic === 'status') {
        console.log('status', message.toString())
        deviceStatus.lastUpdated = new Date();
        // MOCKED DATA
        deviceStatus.capacity = 0;
        deviceStatus.humidity = 0;
        deviceStatus.temperature = 0;
    }
    if (topic === 'getStatus') {
        console.log('getStatus')
        client.publish('status', 'on');
    }
})

export default client;