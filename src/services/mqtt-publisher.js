import deviceStatus from "./device-status.js";
import client from "./mqtt-client.js";

function turnOn() {
    console.log('turn on device')
    if (!client.connected) {
        throw new Error('Client disconnected');
    }
    if (deviceStatus.capacity === null || deviceStatus.lastUpdated === null
        || deviceStatus.capacity < 20 || deviceStatus.lastUpdated < new Date().getTime() - 90000) {
        throw new Error('Turn on failed');
    }
    client.publish('turnOn', '');
}

export default { turnOn };