import { Router } from 'express';
import { getTemperatureController } from '../controllers/get-temperature-controller.js';
const gettemperature = Router();

gettemperature.get('/temperature', getTemperatureController);

export default gettemperature;