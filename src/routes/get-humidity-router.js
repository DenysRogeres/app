import { Router } from 'express';
import { getHumidityController } from '../controllers/get-humidity-controller.js';
const gethumidity = Router();

gethumidity.get('/humidity', getHumidityController);

export default gethumidity;