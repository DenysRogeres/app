import { Router } from 'express';
import turnController from '../controllers/turn-controller.js';

const turnrouter = Router();

turnrouter
.post('/turn', turnController)

export default turnrouter;
