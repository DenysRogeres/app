import express from "express";
import dotenv from "dotenv";

import turnrouter from "./routes/turn-route.js";
import gethumidity from "./routes/get-humidity-router.js";
import gettemperature from "./routes/get-temperature-route.js";

dotenv.config();
const app = express();
app.use(express.json());
app.use(turnrouter);
app.use(gethumidity);
app.use(gettemperature);


const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Servidor Node.js está em execução na porta ${PORT}`);
});

