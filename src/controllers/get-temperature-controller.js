import deviceStatus from "../services/device-status.js";

function getTemperatureController(req, res) {
    const temperature = deviceStatus.temperature;
    if (temperature !== undefined) {
        res.json({ temperature: temperature });
    } else {
        res.status(500).json({ error: 'Umidade não disponível' });
    }
}

export { getTemperatureController };