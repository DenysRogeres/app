import mqttPublisher from "../services/mqtt-publisher.js";

const turnController = (req, res) => {
    try {
        mqttPublisher.turnOn();
        res.status(200).send(`Message sent to turn topic!`);
    }
    catch (error) {
        res.status(500).send(error.message);
    }
};

export default turnController;