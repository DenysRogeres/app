import deviceStatus from "../services/device-status.js";

function getHumidityController(req, res) {
    const humidity = deviceStatus.humidity;
    if (humidity !== undefined) {
        res.json({ humidity: humidity });
    } else {
        res.status(500).json({ error: 'Umidade não disponível' });
    }
}

export { getHumidityController };